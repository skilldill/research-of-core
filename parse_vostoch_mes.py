import os
import shutil
import xlrd
import openpyxl

import pandas as pd
from utils import compare_fields, make_chart, select


print("Подготовка данных")

vmDf = pd.read_excel('./data_files/Восточно_Месояхское/Восточно_Месояхское.xlsx')

fields = ['porositys_water','porosity_hel','porosity_ker']

need_field = compare_fields(vmDf, fields)
fields = list(vmDf['fields'])
litologys = list(vmDf['litologys'])
wells = list(vmDf['wells'])
porositys = list(vmDf[need_field])
permeabi_klenk = list(vmDf['permeabi_klenk'])
well = list(vmDf['well'])

newDf = pd.DataFrame({
    'fields': fields,
    'litologys': litologys,
    'wells': wells,
    'porositys': porositys,
    'permeabi_klenk': permeabi_klenk,
    'well': well
})
print("Таблица собрана")

def sort_by_well():
    shutil.rmtree('./data_files/ВМ', ignore_errors=True)
    current_well = ""

    for well in newDf['well']:
        if well != current_well:
            print(f'{well} Обрабатывается')
            current_well = well
            df = select(newDf, 'well', well)
            chart = make_chart(df["porositys"], df["permeabi_klenk"])

            if len(chart['x']) >0:
                os.makedirs(f'./data_files/ВМ/sort_wells/{well}')
                open(f'./data_files/ВМ/sort_wells/{well}/{well}.xlsx', 'a').close()
                open('./data_files/ВМ/sort_wells/СВОДКА.txt', 'a').close()
                writer = pd.ExcelWriter(f'./data_files/ВМ/sort_wells/{well}/{well}.xlsx')
                df_chart = pd.DataFrame({
                    "porositys": chart["x"],
                    "permeabi_klenk": chart["y"]
                })
                df_chart.to_excel(writer, 'Sheet1')
                writer.save()

                with open('./data_files/ВМ/sort_wells/СВОДКА.txt', 'a') as f:
                    f.write(f'{well} имеет {str(len(chart["x"]))} точек\n')

                print(f'{well} Завершено\n')

            else:
                print(f'{well} Завершено ТОЧЕК НЕТУ\n')
                continue

        else:
            continue

sort_by_well()

# chart = make_chart(newDf["porositys"], newDf["permeabi_klenk"])
#
# df_chart = pd.DataFrame({
#     "porositys": chart["x"],
#     "permeabi_klenk": chart["y"]
# })
#
# print("Подготовка файла")
#
# os.remove('./data_files/Восточно_Месояхское/Обработанное_Восточно_Месояхское.xlsx')
# open('./data_files/Восточно_Месояхское/Обработанное_Восточно_Месояхское.xlsx', 'a').close()
# writer = pd.ExcelWriter(f'./data_files/Восточно_Месояхское/Обработанное_Восточно_Месояхское.xlsx')
# newDf.to_excel(writer, 'Sheet1')
# df_chart.to_excel(writer, "Sheet2")
# writer.save()