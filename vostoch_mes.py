import os
import xlrd
import openpyxl

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from utils import select

print("Подготовка данных")
fesDf = pd.read_excel('./data_files/fes.xlsx')

fields = list(fesDf['field'])
litologys = list(fesDf['Литология.txt'])
wells = list(fesDf['well'])

porositys_water = list(fesDf['Пористость_вода.txt'])
porosity_hel = list(fesDf['Пористость_гелий.txt'])
porosity_ker = list(fesDf['Пористость_керосин.txt'])
effPporositys = list(fesDf['Эфф_порист.txt'])

permeabi_paral = list(fesDf['Прониц_парал.txt'])
permeabi_per = list(fesDf['Прониц_перпенд.txt'])
permeabi_klenk = list(fesDf['Прониц_клинкен.txt'])

well = list(fesDf['well'])

newDf = pd.DataFrame({
    'fields': fields,
    'litologys': litologys,
    'wells': wells,
    'porositys_water': porositys_water,
    'porosity_hel': porosity_hel,
    'porosity_ker': porosity_ker,
    'eff_porositys': effPporositys,
    'permeabi_klenk': permeabi_klenk,
    'permeabi_paral': permeabi_paral,
    'permeabi_per': permeabi_per,
    'well': well
})

vostoch_mes = select(newDf, 'fields','Восточно-Мессояхская')

os.remove('./data_files/Восточно_Месояхское/Восточно_Месояхское.xlsx')
open(f'./data_files/Восточно_Месояхское/Восточно_Месояхское.xlsx', 'a').close()
writer = pd.ExcelWriter(f'./data_files/Восточно_Месояхское/Восточно_Месояхское.xlsx')
vostoch_mes.to_excel(writer, 'Sheet1')
writer.save()