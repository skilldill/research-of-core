import os
import pandas as pd
import numpy as np

def select(d_frame, column, value):
    '''Выгружает необходимые строки по полям'''
    return d_frame.loc[d_frame[column] == value]


def get_fill_cells(field):
    '''Собирает массив без пустых полей'''
    return  [cell for cell in field if not cell == ""]


def compare_fields(d_frame, fields):
    '''Сравниает поля по размеру не пустых значений'''
    obj_fields = []
    for field in fields:
        obj_fields.append(dict(field=field, values=get_fill_cells(d_frame[field])))

    biggest_obj = dict(field="", values=pd.Series([]))

    type(obj_fields[0]["values"])

    for obj in obj_fields:
        if len(obj['values']) > len(biggest_obj['values']):
            biggest_obj = obj

    return obj["field"]


def make_chart(x_seria, y_seria):
    '''
        Идёт по двум массивам и если в каком то из массивов
        элемент пустой, пустой элемент и элемент с таким же индексом
        в другом массиве не добавляются в массив точек
    '''
    print("Подготовка точек для графика")
    chart = dict(x=[], y=[], ln_x=[], ln_y=[])

    x_list = list(x_seria)
    y_list = list(y_seria)

    len_series = len(x_list)
    print(f"Количество необработанных точеек {len_series}")

    for i in range(len_series):
        if  x_list[i] > 0 and y_list[i] > 0:
            chart["x"].append(x_list[i])
            chart["y"].append(y_list[i])
            chart["ln_x"].append(np.log(x_list[i]))
            chart["ln_y"].append(np.log(y_list[i]))

    print(f"Количество обработанных точеек {len(chart['x'])}")
    print("Точки собраны")

    return chart


def least_square(x_dots, y_dots):
    '''Метод наименьших квадратов'''
    n = len(x_dots)
    sum_xy = 0
    sum_x = 0
    sum_y = 0
    sum_x_squer = 0

    for i in range(n):
        sum_xy += x_dots[i] * y_dots[i]
        sum_x += x_dots[i]
        sum_y += y_dots[i]
        sum_x_squer += x_dots[i] * x_dots[i]

    sum_squer_x = sum_x * sum_x

    a = (sum_xy - sum_x * sum_y)/(sum_x_squer - sum_squer_x)
    b = (sum_y - a * sum_x)/n

    return dict(a=a, b=b)

def sort_by_well(df, field):
    current_well = ""

    for well in df['well']:
        if not well == current_well:
            print(f'{well} Обрабатывается')

            current_well = well

            well_df = select(df, 'well', well)

            try:
                chart = make_chart(well_df["porositys_water"], well_df["permeabi_klenk"])

                if len(chart['x']) > 0:
                    os.makedirs(f'./data_files/results/sort_fields/{field}/sort_wells/{well}')
                    open(f'./data_files/results/sort_fields/{field}/sort_wells/{well}/{well}.xlsx', 'a').close()
                    open(f'./data_files/results/sort_fields/{field}/sort_wells/СВОДКА.txt', 'a').close()
                    writer = pd.ExcelWriter(f'./data_files/results/sort_fields/{field}/sort_wells/{well}/{well}.xlsx')

                    df_chart = pd.DataFrame({
                        "porositys": chart["x"],
                        "permeabi_klenk": chart["y"],
                        "ln_porositys": chart['ln_x'],
                        "ln_permeabi_klenk": chart['ln_y']
                    })

                    df_chart.to_excel(writer, 'Sheet1')
                    writer.save()

                    first_method = least_square(chart['ln_x'], chart['ln_y'])
                    second_method = least_square(chart['x'], chart['ln_y'])

                    with open(f'./data_files/results/sort_fields/{field}/sort_wells/{well}/{well}.txt', "a") as f:
                        f.write(f"""Первым способом полученые коэффициенты равны: 
                                а0:{first_method['a']}, a1:{first_method['b']}\n
                                Вторым способом полученные коэфициенты равны:
                                а0:{second_method['a']}, a1:{second_method['b']}\n""")

                    with open(f'./data_files/results/sort_fields/{field}/sort_wells/СВОДКА.txt', 'a') as f:
                        f.write(f'{well} имеет {str(len(chart["x"]))} точек\n')

                    print(f'{well} Завершено\n')

                else:
                    print(f'{well} Завершено ТОЧЕК НЕТУ\n')
                    continue

            except:
                with open('./data_files/results/sort_fields/ERRORS.txt', 'a') as f:
                    f.write(f'При обработке {well} в {field} произошла ошибка\n')

        else:
            continue